## Updating requirements:
In case of adding new dependencies to our project

1. Add the package name to [/src/base_requirements.txt](/src/base_requirements.txt)

2. Run bash script `./scripts/update_requirements`

## DB MIGRATIONS

### Creating magrations:

Inside a container `winter-api` run:

#### Adding extension:
```python
def upgrade():
    connection = op.get_bind()
    connection.execute('CREATE EXTENSION IF NOT EXISTS "postgis";')
    # uuid_generate_v4() is not available by default
    connection.execute('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";')
```

```bash
# 1. To generate new migration
alembic revision --autogenerate -m "your txt here"
# 2. To update DB
alembic upgrade head
```

#### UNDO migration

```bash
# To downgrade 1 migration back
alembic downgrade -1
```

#### Problems with test database:
 ```bash
# Remove test postgres container: 
 docker rm -fv test-postgres
 ```