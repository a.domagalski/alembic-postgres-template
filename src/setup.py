from pip._internal.req import parse_requirements
from setuptools import find_packages
from setuptools import setup


setup(
    name='app',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        str(ir.req) for ir in parse_requirements('base_requirements.txt', session=False)
    ],
    entry_points={
        'console_scripts': [
            'migrate = app.commands.migrate:run',
        ]
    },
)
