from enum import IntEnum
import logging
import sqlalchemy as sa
from app import database

logger = logging.getLogger(__name__)


class BaseModel:
    id = sa.Column(sa.Integer, primary_key=True)
    external_id = sa.Column(sa.String, unique=True)

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__}({self.__dict__})>'

class EventEnum(IntEnum):
    FOO = 1
    BAZ = 2


# Deprecated
class Event(database.db.Model, BaseModel):
    __tablename__ = 'event'

    name = sa.Column(sa.String)
    event_type = sa.Column(sa.Enum(EventEnum))
    start_org_id = sa.Column(sa.String)
    end_org_id = sa.Column(sa.String)
    start_individual_id = sa.Column(sa.String)
    end_individual_id = sa.Column(sa.String)
    start_topic_id = sa.Column(sa.String)
    end_topic_id = sa.Column(sa.String)

class Event(database.db.Model, BaseModel):
    __tablename__ = 'event_siema'

    name = sa.Column(sa.String)
    event_type = sa.Column(sa.Enum(EventEnum))
